package com.mediamonks.recaptchav3;

import java.io.IOException;
import java.util.Dictionary;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.OsgiUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Create a servlet to verify the token that is generated in the front end 
 * with google server by passing the secret key.
 */
@SlingServlet(paths = "/bin/mclaren/recaptchaservlet", methods = "POST", metatype = true)
@Properties({
		@org.apache.felix.scr.annotations.Property(name = "SECRET-KEY", description = "Secret key", value = "Secret key"),
		@org.apache.felix.scr.annotations.Property(name = "SITE-KEY", description = "Site key", value = "Site key") })

public class RecaptchaServlet extends SlingAllMethodsServlet {
	private static final String RESPONSETYPE = "application/json";
	private static final String UTF8 = "UTF-8";

	/**
	 * General serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private final Logger log = LoggerFactory.getLogger(RecaptchaServlet.class);

	/**
	 * Recaptcha Site key.
	 */
	private String strSiteKey;

	/**
	 * Recaptcha Secret key.
	 */
	private String strSecretKey;

	public static final String SECRET_KEY = "SECRET-KEY";
	public static final String SITE_KEY = "SITE-KEY";

	protected void activate(ComponentContext componentContext) {
		configure(componentContext.getProperties());
		log.info("strSecretKey=" + this.strSecretKey);
		log.info("strSiteKey=" + this.strSiteKey);

	}

	protected void configure(Dictionary<?, ?> properties) {
		this.strSecretKey = OsgiUtil.toString(properties.get(SECRET_KEY), null);
		this.strSiteKey = OsgiUtil.toString(properties.get(SITE_KEY), null);
	}

	@Override
	protected void doPost(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
			throws ServletException, IOException {

		log.info(":::::::::::::: RecaptchaServlet POST() Starts :::::::::::::");
		log.info("strSecretKey=" + this.strSecretKey);
		log.info("strSiteKey=" + this.strSiteKey);

		String response = "";
		response = getResponse(req);
		resp.setContentType(RESPONSETYPE);
		resp.getWriter().write(response);
		log.info(":::::::::::::: RecaptchaServlet PPOST() Ends :::::::::::::");
	}

	public String getResponse(SlingHttpServletRequest req) {
		String response = "[]";
		String str = req.getParameter("g-recaptcha-response") != null ? req.getParameter("g-recaptcha-response") : "";
		String configUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + strSecretKey + "&response="
				+ str;
		try {
			log.info("::: Params :: token:: " + str);
			if (str != null) {
				String baseUrl = configUrl;
				CloseableHttpClient client = null;
				HttpClientBuilder httpClientBuilder = null;
				httpClientBuilder = HttpClientBuilder.create();
				client = httpClientBuilder.build();
				HttpGet request = new HttpGet(baseUrl);
				HttpResponse response1 = client.execute(request);
				if (response1 != null && response1.getEntity() != null) {
					HttpEntity entity = response1.getEntity();
					response = EntityUtils.toString(entity, UTF8);
				}
				log.info("Response::: " + response);
			}
		} catch (Exception e) {
			log.error("Error in processing http request ::: ");
		}
		return response;
	}

}
