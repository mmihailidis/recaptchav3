// Validate google server response by sending the request to AEM servlet.
// Discard all the response which has score less than 0.5

function recaptchav3(){

    var nop = false;

	if ($("#g-recaptcha-response").length) {

		var grecap = $("#g-recaptcha-response").val();

	  	$.ajax({
	      	method: "POST",
			url: '/bin/mclaren/recaptchaservlet?g-recaptcha-response=' + grecap,
			data: $(this).serialize(), 
			async: false, 
			dataType: 'json', 
			cache: false, 
			success: function(data) {
				if (data.success === true && data.score > 0.5) {
					nop = true;  
				} else {   
					nop = false;  
				} 
			}, 
			fail: function(textStatus, errorThrown) {  
				console.log(textStatus, errorThrown); 
			}
		});

	}

    return nop;

};